
import { combineReducers } from 'redux';

import homePageReducer from './home/home.page.reducer';

export default combineReducers({
  home: homePageReducer,
});
