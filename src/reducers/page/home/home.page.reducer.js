
import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  updateHomePage: (payload = { }) => ({
    payload,
    type: 'UPDATE_HOME_PAGE',
  }),
});

const INITIAL_STATE = {
  sectionVisibility: {
    about: false,
    freshExperience: false,
    jobHistory: false,
    languageHistory: false,
    toolingHistory: false,
  },
};

const updateHomePage = (state = INITIAL_STATE, action) => ({
  ...state,
  ...action.payload,
  sectionVisibility: {
    ...state.sectionVisibility,
    ...action.payload.sectionVisibility,
  },
});

export default createReducer(INITIAL_STATE, {
  [Types.UPDATE_HOME_PAGE]: updateHomePage,
});
