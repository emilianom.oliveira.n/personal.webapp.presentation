
import React from 'react';
import { ResponsiveLine } from '@nivo/line';
import propTypes from 'prop-types';

const LineChart = ({ data }) => (
  <ResponsiveLine
    enablePoints={ true }
    data={ data }
    margin={{
      bottom: 30,
      left: 50,
      right: 20,
      top: 20,
    }}
    enableSlices="x"
    xScale={{
      stacked: false,
      type: 'point',
    }}
    yScale={{
      max: 'auto',
      min: 'auto',
      reverse: false,
      stacked: false,
      type: 'linear',
    }}
    axisTop={ null }
    axisRight={ null }
    curve="monotoneX"
    axisLeft={{
      legend: 'years of experience',
      legendOffset: -40,
      legendPosition: 'middle',
      orient: 'left',
      tickPadding: 5,
      tickRotation: 0,
      tickSize: 5,
    }}
    colors={ { scheme: 'nivo' } }
    pointSize={ 10 }
    pointColor={ {
      from: 'color',
      modifiers: [ ],
    } }
    pointBorderWidth={ 2 }
    pointBorderColor={ {
      from: 'serieColor',
    } }
    pointLabel="y"
    pointLabelYOffset={ 0 }
    useMesh
    motionStiffness={ 200 }
  />
);

LineChart.propTypes = {
  data: propTypes.arrayOf(propTypes.shape({
    data: propTypes.arrayOf(propTypes.shape({
      x: propTypes.number,
      y: propTypes.number,
    })),
    id: propTypes.string,
  })).isRequired,
};

export default LineChart;
