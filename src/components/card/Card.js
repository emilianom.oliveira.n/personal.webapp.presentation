
import React, { memo } from 'react';
import styled, { css } from 'styled-components';
import propTypes from 'prop-types';
import { transitions } from 'polished';

import { COLOR_1 } from '../../styled/variables/variables.styled';
import { COLOR_3 } from '../../styled/variables/variables.styled';
import { Divider } from '../../styled/components/components.styled';
import { SecondaryLabel } from '../../styled/labels/labels.styled';
import { deepCompare } from '../../utils/object/object.util';
import { shadow } from '../../styled/helpers/helpers.styled';

const Card = ({
  children,
  className,
  icon,
  show,
  title,
}) => (
  <Wrapper
    className={ className }
    visible={ show }
  >
    { title && (
      <TitleWrapper>
        { icon && <Icon as={ icon } /> }
        <Title>
          { title }
        </Title>
      </TitleWrapper>
    ) }
    { title && <Divider /> }
    <ContentWrapper>
      { children }
    </ContentWrapper>
  </Wrapper>
);

const Wrapper = styled.div`
  background-color: ${ COLOR_1 };
  border-radius: 10px;
  ${ shadow({ direction: 'bottom-right' }) }
  opacity: 0;
  overflow: hidden;

  padding: 20px;
  ${ transitions([ 'opacity' ], 'ease .6s') }
  ${ ({ visible }) => visible && css`
      opacity: 1;
  ` }
`;

const TitleWrapper = styled.div`
  flex-direction: row;
`;

const Icon = styled.svg`
  align-self: center;
  color: ${ COLOR_3 };
  height: 16px;
  margin-right: 10px;
`;

const Title = styled(SecondaryLabel)`
  align-self: center;
  font-size: 14px;
  text-transform: uppercase;
`;

const ContentWrapper = styled.div`
  width: 100%;
`;

Card.propTypes = {
  children: propTypes.any,
  className: propTypes.string,
  icon: propTypes.object,
  show: propTypes.bool,
  title: propTypes.string,
};

Card.defaultProps = {
  show: true,
};

export default memo(Card, deepCompare);
