
import SweetScroll from 'sweet-scroll';

const DEFAULT_OPTIONS = {
  duration: 1000,
  easing: 'easeInOutQuad',
};

export const scrollToTop = ({
  container = window,
  options = { },
} = {
  container: window,
}) => {
  const scroller = new SweetScroll({
    ...DEFAULT_OPTIONS,
    offset: 0,
    ...options,
  }, container);
  scroller.to(0);

  return scroller;
};

export const scrollTo = ({
  container = window,
  position = 0,
  options = { },
}) => {
  const scroller = new SweetScroll({
    ...DEFAULT_OPTIONS,
    offset: window.innerWidth >= 768 ? -100 : -80,
    ...options,
  }, container);
  scroller.to(position);
  return scroller;
};

export const scrollToElement = ({
  container = window,
  element,
  options = { },
}) => {
  if(typeof element === 'string') {
    element = document.getElementById(element);
  }

  const scroller = new SweetScroll({
    ...DEFAULT_OPTIONS,
    offset: window.innerWidth >= 768 ? -100 : -80,
    ...options,
  }, container);
  scroller.toElement(element);
  return scroller;
};
