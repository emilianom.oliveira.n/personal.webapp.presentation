
const REACT_JS = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0.5,
  }, {
    x: 2020,
    y: 1.5,
  }, {
    x: 2021,
    y: 2.5,
  }, {
    x: 2022,
    y: 2.75,
  }],
  id: 'React JS',
};

const VUE_JS = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 0,
  }, {
    x: 2021,
    y: 1,
  }, {
    x: 2022,
    y: 1,
  }],
  id: 'Vue JS',
};

const REACT_NATIVE = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 0.5,
  }, {
    x: 2021,
    y: 1.5,
  }, {
    x: 2022,
    y: 1.75,
  }],
  id: 'React Native',
};

const FLUTTER = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 0,
  }, {
    x: 2020,
    y: 0,
  }, {
    x: 2022,
    y: 0.25,
  }],
  id: 'Flutter',
};

const ANGULAR_JS = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 1,
  }, {
    x: 2018,
    y: 2,
  }, {
    x: 2019,
    y: 2,
  }, {
    x: 2020,
    y: 2,
  }, {
    x: 2021,
    y: 2,
  }, {
    x: 2022,
    y: 2,
  }],
  id: 'Angular JS',
};

const NODE_JS = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 1,
  }, {
    x: 2021,
    y: 2,
  }, {
    x: 2022,
    y: 2.25,
  }],
  id: 'Node JS',
};

const ANGULAR = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0.5,
  }, {
    x: 2019,
    y: 0.5,
  }, {
    x: 2020,
    y: 1.5,
  }, {
    x: 2021,
    y: 1.5,
  }, {
    x: 2022,
    y: 1.5,
  }],
  id: 'Angular',
};

const MONGODB = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 1,
  }, {
    x: 2021,
    y: 2,
  }, {
    x: 2022,
    y: 2.25,
  }],
  id: 'MongoDB',
};

const XAMARIN = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0.5,
  }, {
    x: 2018,
    y: 0.5,
  }, {
    x: 2019,
    y: 0.5,
  }, {
    x: 2020,
    y: 0.5,
  }, {
    x: 2021,
    y: 0.5,
  }, {
    x: 2022,
    y: 0.5,
  }],
  id: 'Xamarin',
};

const DOTNET_CORE = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0.25,
  }, {
    x: 2019,
    y: 0.25,
  }, {
    x: 2020,
    y: 0.25,
  }, {
    x: 2021,
    y: 0.25,
  }, {
    x: 2022,
    y: 0.25,
  }],
  id: 'Dotnet Core',
};

const SPRING = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 0,
  }, {
    x: 2021,
    y: 0.5,
  }, {
    x: 2022,
    y: 0.5,
  }],
  id: 'Spring',
};

export const TOOLING_HISTORY = [
  ANGULAR_JS,
  ANGULAR,
  DOTNET_CORE,
  FLUTTER,
  MONGODB,
  NODE_JS,
  REACT_JS,
  REACT_NATIVE,
  SPRING,
  VUE_JS,
  XAMARIN,
].sort((a, b) => {
  const first = a.data[a.data.length - 1].y;
  const second = b.data[b.data.length - 1].y;
  return first - second;
});
