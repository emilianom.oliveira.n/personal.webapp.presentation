
import React, { memo, useState } from 'react';
import { ChartBar } from '@styled-icons/fa-regular';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import Card from '../../../components/card/Card';
import { Creators as HomePageReducerActions } from '../../../reducers/page/home/home.page.reducer';
import LineChart from '../../../components/line-chart/LineChart';
import { TOOLING_HISTORY } from './constants/tooling-history.constants';
import Visibility from '../../../hocs/visibility/Visibility';
import { bindActionCreators } from 'redux';
import { deepCompare } from '../../../utils/object/object.util';

export const ID = 'tooling_history_section';

const ToolingHistory = () => {

  const { updateHomePage } = bindActionCreators({
    updateHomePage: HomePageReducerActions.updateHomePage,
  }, useDispatch());

  const [ state, setState ] = useState({
    show: false,
  });

  const handleVisibilityChange = isVisible => {
    updateHomePage({
      sectionVisibility: {
        toolingHistory: isVisible,
      },
    });

    if(!isVisible) return;
    setState(prev => ({
      ...prev,
      show: isVisible,
    }));
  };

  return (
    <Wrapper>
      <Visibility onChange={ handleVisibilityChange }>
        <THCard show={ state.show }>
          <ContentWrapper>
            <LineChart data={ TOOLING_HISTORY } />
          </ContentWrapper>
        </THCard>
      </Visibility>
    </Wrapper>
  );
};

const Wrapper = styled.section.attrs({
  id: ID,
})`
  margin-bottom: 30px;
`;

const THCard = styled(Card).attrs({
  icon: ChartBar,
  title: 'tooling history',
})``;

const ContentWrapper = styled.div`
  height: 400px;
  width: 100%;
`;

export default memo(ToolingHistory, deepCompare);
