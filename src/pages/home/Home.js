
import React, { memo } from 'react';
import styled from 'styled-components';

import About from './about/About';
import FreshExperience from './fresh-experience/FreshExperience';
import JobHistory from './job-history/JobHistory';
import LanguageHistory from './language-history/LanguageHistory';
import Sidebar from './sidebar/Sidebar';
import ToolingHistory from './tooling-history/ToolingHistory';
import { deepCompare } from '../../utils/object/object.util';
import { mqTablet } from '../../styled/helpers/helpers.styled';
import { pageBase } from '../../styled/blocks/blocks.styled';

const Home = () => (
  <Wrapper>
    <About />
    <FreshExperience />
    <LanguageHistory />
    <ToolingHistory />
    <JobHistory />
    <Sidebar />
  </Wrapper>
);

const Wrapper = styled.div`
  ${ pageBase() }
  padding-left: 30px;
  
  ${ mqTablet`
    padding-left: 130px;
  ` }
`;

export default memo(Home, deepCompare);
