
export const JOB_HISTORY = [{
  company: 'Platform Builders',
  description: `Here at Platform Builders I've initiated the journey as a developer for one of our biggest clients (names cannot be disclosed). During this period I was solely responsible for handling two projects and for developing our architectural guides (for the client) from scratch. Right now I work as a Cross Developer (Which would be analogous to a "Frontend Tech Lead") on 2 clients, as a project lead on a internal product that helps code review automation, help lead architectural discussions regarding our internal guides and brought a personally developed product/startup to our innovation business unit, which is now under the assessment phase.`,
  duration: {
    from: new Date('2020-12-01'),
    to: null,
  },
  id: 'platform_builders',
  place: {
    city: 'São Paulo',
    country: 'Brazil',
    state: 'SP',
  },
  title: 'Senior Software Engineer',
  tools: [
    'React',
    'React Native',
    'MobX',
    'Styled Components',
    'Typescript',
  ],
  website: 'https://www.platformbuilders.io',
}, {
  company: 'M4U',
  description: `At M4U I am in one of the core teams of the company. Like such, we develop core products like payment options integrations (like Paypal, Apple Pay, GPay), to facilitate the use by other teams.
  <br />
  <br />
  We've also developed a fully customizable form that runs within the PCI scope (Payment Card Industry Data Security Standards) to be integrated on all the relevant clients so they can be removed from the scope and still collect the customer's card info in a controlled and secure environment.`,
  duration: {
    from: new Date('2020-05-01'),
    to: new Date('2020-12-01'),
  },
  id: 'm4u',
  place: {
    city: 'Petrópolis',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Senior Software Engineer',
  tools: [
    'NodeJS',
    'Lambda',
    'MySQL',
    'Vue',
    'VueX',
  ],
  website: 'https://cogoli.io',
}, {
  company: 'Cogoli Engenharia de Software',
  description: `At Cogoli we are a team of 3. One designer and two engineers with me in the frontline.
  <br />
  <br />
  By now we've worked on basically two products starting from scratch. The first one being a real estate advertising platform built entirely with React with some good use of Google Maps. And the second one being a medicine order/delivery app, including the drugstore platform, the support platform, the backend service, database and of course the mobile app.`,
  duration: {
    from: new Date('2019-04-01'),
    to: new Date(),
  },
  id: 'cogoli',
  place: {
    city: 'Petrópolis',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Senior Software Engineer',
  tools: [
    'React',
    'React Native',
    'Redux',
    'Saga',
    'Styled Components',
    'React',
    'NodeJS',
    'Express',
    'MongoDB',
  ],
  website: 'https://cogoli.io',
}, {
  company: 'Wooza Telecom',
  description: `Now in this second opportunity, I've got into the Claro (other telecom company) team.
  <br />
  <br />
  A team with a fairly similar stack in old projects but quite distinctive on the most recent ones. Here I've worked mostly with React and helping to introduce Redux and Styled components to the team, which allowed an amazing code quality, organization and reuse. Since then I'm a huge fan of the React paradigm which became my go-to framework.`,
  duration: {
    from: new Date('2018-11-01'),
    to: new Date('2019-02-01'),
  },
  id: 'wooza_telecom',
  place: {
    city: 'Rio de Janeiro',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Senior Front-End Engineer',
  tools: [
    'React',
    'AngularJs',
    'Angular 4',
    'Typescript',
  ],
  website: 'https://www.wooza.com.br',
}, {
  company: 'BTG Pactual',
  description: `At BTG I worked at an AML Project (Anti Money Laundering). Although I was hired to work on the backend side as well, the client needed to be completely refactored. Me being the most experienced on the frontend side, no time to split attention was found.
  <br />
  <br />
  The project had huge complexity, ingesting data from various sources and showing them in a clear and friendly manner to the end user. I've also implemented a graph for the project using Cytoscape, which got the attention from other areas and was in the process of being converted into an internal product.
  <br />
  <br />
  Considering the particularities of the project and the fact that BTG is the biggest investment bank in Latin America, I'm very proud of my position there.`,
  duration: {
    from: new Date('2018-05-01'),
    to: new Date('2018-11-01'),
  },
  id: 'btg_pactual',
  place: {
    city: 'Rio de Janeiro',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Senior Software Engineer',
  tools: [
    'Angular 6',
    'Typescript',
    'CytoscapeJs',
  ],
  website: 'https://www.btgpactual.com',
}, {
  company: 'Wooza',
  description: `Wooza is a company specialised in digital marketing and online sales. We had contracts with the 4 biggest Telecom companies in the country. That being said, everything you do at Wooza will probably be used by millions of people.

  There I was responsible for developing checkouts to facilitate the acquisition of telecom products in the Vivo (telecom company) team. I also had the opportunity to contribute on modernizing our products by helping to introduce Angular 4 and Typescript to our arsenal.`,
  duration: {
    from: new Date('2017-04-01'),
    to: new Date('2017-12-01'),
  },
  id: 'wooza',
  place: {
    city: 'Rio de Janeiro',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Senior Front-End Engineer',
  tools: [
    'AngularJs',
    'Angular 2',
    'Typescript',
  ],
  website: 'https://www.wooza.com.br',
}, {
  company: 'Medicinow',
  description: `Medicinow was my first real project. I had worked before on two other companies with Xamarin and Ionic as an Intern and even before with Erlang in a Scientific Initiation Program, but not with huge responsibilities was we should expect. I wanted more.
  <br />
  <br />
  That's when I initiated the Medicinow project. I wanted to be overwhelmed with responsibilities since that's how I believe humans really grow. With that in mind I was responsible for the architecture of all our solutions as well as development. I had the opportunity to work with Ruby on Rails, Django, Tastypie, MySQL, Electron, AWS and usual front-end web technologies. Me and my partners were able to achieve good results, but financial necessity hit and I had to leave the project.
  <br />
  <br />
  By the end all of this work gave me the necessary experience to get back into the market in a good position as a Senior Front-End developer. Of course I still had to polish some practices and learn some new tools, but the challenge was met and there I officially began my career.`,
  duration: {
    from: new Date('2016-05-01'),
    to: new Date('2017-04-01'),
  },
  id: 'medicinow',
  place: {
    city: 'Petrópolis',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Software Engineer',
  tools: [
    'AngularJs',
    'Ionic',
    'Flask',
    'Python',
  ],
  website: 'https://www.medicinow.com',
}, {
  company: 'Spinpo',
  description: `This was by far the best internship I've had. Since being a small company, I had a fairly good amount of responsibilities for my given position. Unfortunately it had to close doors not long after I got in.
  <br />
  <br />
  There I've worked developing client-side applications, such as websites and mobile apps. To achieve our goals I’ve basically AngularJs and the Ionic Framework.
  <br />
  <br />
  Great people with great work ethic. It was a very intense and satisfying experience working at Spinpo.`,
  duration: {
    from: new Date('2016-02-01'),
    to: new Date('2016-05-01'),
  },
  id: 'spinpo',
  place: {
    city: 'Petrópolis',
    country: 'Brazil',
    state: 'RJ',
  },
  title: 'Software Engineer Intern',
  tools: ['Javascript', 'Ionic'],
  website: 'https://www.spinpo.com',
}];
