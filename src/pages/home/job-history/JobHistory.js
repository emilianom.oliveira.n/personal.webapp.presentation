
import React, { Fragment, memo, useState } from 'react';
import { Building } from 'styled-icons/fa-regular';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import Card from '../../../components/card/Card';
import { Divider } from '../../../styled/components/components.styled';
import { Creators as HomePageReducerActions } from '../../../reducers/page/home/home.page.reducer';
import { JOB_HISTORY } from './constants/job-history.constants';
import Job from './job/Job';
import Visibility from '../../../hocs/visibility/Visibility';
import { deepCompare } from '../../../utils/object/object.util';

export const ID = 'job_history_section';

const JobHistory = () => {

  const { updateHomePage } = bindActionCreators({
    updateHomePage: HomePageReducerActions.updateHomePage,
  }, useDispatch());

  const [ state, setState ] = useState({
    show: false,
  });

  const handleVisibilityChange = isVisible => {
    updateHomePage({
      sectionVisibility: {
        jobHistory: isVisible,
      },
    });

    if(!isVisible) return;
    setState(prev => ({
      ...prev,
      show: isVisible,
    }));
  };

  return (
    <Wrapper>
      <Visibility onChange={ handleVisibilityChange }>
        <JHCard show={ state.show }>
          { JOB_HISTORY.map((j, index, list) => (
            <Fragment key={ j.id }>
              <Job { ...j } />
              { index < (list.length - 1) && <Divider /> }
            </Fragment>
          )) }
        </JHCard>
      </Visibility>
    </Wrapper>
  );
};

const Wrapper = styled.section.attrs({
  id: ID,
})``;

const JHCard = styled(Card).attrs({
  icon: Building,
  title: 'job history',
})``;

export default memo(JobHistory, deepCompare);
