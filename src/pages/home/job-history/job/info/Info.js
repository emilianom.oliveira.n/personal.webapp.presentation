
import React, { memo } from 'react';
import styled, { css } from 'styled-components';
import { Circle } from '@styled-icons/fa-regular';
import propTypes from 'prop-types';

import { COLOR_3, COLOR_5 } from '../../../../../styled/variables/variables.styled';
import { PrimaryLabel, SecondaryLabel } from '../../../../../styled/labels/labels.styled';
import { deepCompare } from '../../../../../utils/object/object.util';
import { mqTablet } from '../../../../../styled/helpers/helpers.styled';
import { pointer } from '../../../../../styled/blocks/blocks.styled';

const Info = ({
  title,
  company,
  website,
  duration,
  place,
}) => {

  const handleWebsiteClick = () => {
    window.open(website);
  };

  return (
    <Wrapper>
      <Title>
        { title }
      </Title>
      <TopicWrapper>
        <TopicIcon />
        <TopicLabel>
          { company }
        </TopicLabel>
      </TopicWrapper>

      <TopicWrapper onClick={ handleWebsiteClick }>
        <TopicIcon />
        <TopicLabel>
          { website }
        </TopicLabel>
      </TopicWrapper>

      <TopicWrapper>
        <TopicIcon />
        <TopicLabel>
          { place.city } - { place.state }, { place.country }
        </TopicLabel>
      </TopicWrapper>

      <TopicWrapper>
        <TopicIcon />
        <TopicLabel>
          { (() => {

            const { from, to } = duration;
            const fromStrings = from.toString().split(' ');
            let parsed = `${ fromStrings[1] }, ${ fromStrings[3] } to `;

            if(!to) {
              parsed += 'now';
            } else {
              const toStrings = to.toString().split(' ');
              parsed += `${ toStrings[1] }, ${ toStrings[3] }`;
            }

            return parsed;
          })() }
        </TopicLabel>
      </TopicWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin-bottom: 20px;

  ${ mqTablet`
    margin: 0;
    margin-right: 20px;
    min-width: 300px;
  ` }
`;

const Title = styled(PrimaryLabel)`
  margin-bottom: 10px;
`;

const TopicWrapper = styled.div`
  flex-direction: row;
  height: 20px;
  width: fit-content;
  ${ ({ onClick }) => onClick && css`
    & > label {
      text-decoration: underline;
      text-decoration-color: ${ COLOR_5 };
      ${ pointer }
    }
  ` }
`;

const TopicIcon = styled(Circle)`
  align-self: center;
  color: ${ COLOR_3 };
  height: 12px;
  margin-right: 5px;
`;

const TopicLabel = styled(SecondaryLabel)`
  align-self: center;
  font-size: 12px;
  line-height: 12px;
`;

Info.propTypes = {
  company: propTypes.string,
  duration: propTypes.shape({
    from: propTypes.objectOf(Date),
    to: propTypes.oneOfType([
      propTypes.objectOf(Date),
    ]),
  }),
  place: propTypes.shape({
    city: propTypes.string,
    country: propTypes.string,
    state: propTypes.string,
  }),
  title: propTypes.string,
  website: propTypes.string,
};

Info.defaultProps = { };

export default memo(Info, deepCompare);
