
import React, { Fragment, memo } from 'react';

import Info from './info/Info';
import { SecondaryLabel } from '../../../../styled/labels/labels.styled';
import { deepCompare } from '../../../../utils/object/object.util';
import { mqTablet } from '../../../../styled/helpers/helpers.styled';
import propTypes from 'prop-types';
import styled from 'styled-components';

const Job = ({
  company,
  description,
  duration,
  place,
  title,
  tools,
  website,
}) => (
  <Wrapper>
    <Info
      title={ title }
      company={ company }
      website={ website }
      duration={ duration }
      place={ place }
    />
    <Description>
      &quot;
        { description.split('<br />').map((sentence, index, list) => (
          <Fragment key={ index }>
            { sentence }
            { index < list.length - 1 && <br /> }
          </Fragment>
        )) }
      &quot;
      <br />
      <br />
      Main Tools: { tools.join(', ') }.
    </Description>
  </Wrapper>
);

const Wrapper = styled.div`
  ${ mqTablet`
    flex-direction: row;
  ` }
`;

const Description = styled(SecondaryLabel)`
  font-size: 14px;
  font-style: italic;

  ${ mqTablet`
    align-self: center;
  ` }
`;

export const Indicator = styled(Description).attrs({
  as: 'strong',
})``;

Job.propTypes = {
  company: propTypes.string,
  description: propTypes.string,
  duration: propTypes.object,
  place: propTypes.object,
  title: propTypes.string,
  tools: propTypes.arrayOf(propTypes.string),
  website: propTypes.string,
};

Job.defaultProps = { };

export default memo(Job, deepCompare);
