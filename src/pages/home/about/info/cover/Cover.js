
import React, { memo } from 'react';
import styled from 'styled-components';

import { backgrounder, dim } from '../../../../../styled/helpers/helpers.styled';
import { COLOR_1 } from '../../../../../styled/variables/variables.styled';
import { PrimaryLabel } from '../../../../../styled/labels/labels.styled';
import { ProfilePictureMedium } from '../../../../../styled/components/components.styled';
import { buttonHovering } from '../../../../../styled/blocks/blocks.styled';
import { deepCompare } from '../../../../../utils/object/object.util';

const { PUBLIC_URL } = process.env;

const Cover = () => {

  const handleClick = () => {
    const { REACT_APP_LINKED_IN } = process.env;
    window.open(REACT_APP_LINKED_IN);
  };

  return (
    <Wrapper>
      <InnerWrapper onClick={ handleClick }>
        <OpacityLayer />
        <TopWrapperContent>
          <Picture />
          <Name>
            Emiliano Oliveira
          </Name>
          <Profession>
            Software Engineer
          </Profession>
        </TopWrapperContent>
      </InnerWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  height: 200px;
  margin: -20px;
  margin-bottom: 0;
  overflow: hidden;
`;

const InnerWrapper = styled.div`
  ${ backgrounder({
    url: `${ PUBLIC_URL }/images/profile-cover-1920.jpg`,
  }) }
  height: 200px;
  ${ buttonHovering }
`;

const OpacityLayer = styled.div`
  height: 100%;
  width: 100%;
  ${ dim({ }) }
`;

const TopWrapperContent = styled.div`
  height: 100%;
  justify-content: center;
  margin-top: -200px;
  width: 100%;
`;

const Picture = styled(ProfilePictureMedium)`
  align-self: center;
  border: 2px ${ COLOR_1 } solid;
  height: 100px;
  margin-bottom: 20px;
  width: 100px;
`;

const Name = styled(PrimaryLabel)`
  align-self: center;
  color: ${ COLOR_1 };
  font-size: 16px;
`;

const Profession = styled(Name)`
  font-size: 12px;
  opacity: 0.7;
`;

export default memo(Cover, deepCompare);
