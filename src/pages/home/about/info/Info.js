
import React, { memo, useState } from 'react';
// import { GitAlt } from '@styled-icons/fa-brands';
import { Linkedin as  LinkedIn } from '@styled-icons/fa-brands';
import { ListAlt } from '@styled-icons/fa-regular';
import styled from 'styled-components';

import { COLOR_5 } from '../../../../styled/variables/variables.styled';
import Card from '../../../../components/card/Card';
import Cover from './cover/Cover';
import { PrimaryButton } from '../../../../styled/buttons/buttons.styled';
import { PrimaryLabel } from '../../../../styled/labels/labels.styled';
import Visibility from '../../../../hocs/visibility/Visibility';
import { buttonHovering } from '../../../../styled/blocks/blocks.styled';
import { deepCompare } from '../../../../utils/object/object.util';
import { mqDesktop } from '../../../../styled/helpers/helpers.styled';

const Info = () => {

  const [ state, setState ] = useState({
    show: false,
  });

  const handleLinkedInClick = () => {
    const { REACT_APP_LINKED_IN } = process.env;
    window.open(REACT_APP_LINKED_IN);
  };

  const handleResumeClick = () => {
    const { PUBLIC_URL } = process.env;
    window.open(`${ PUBLIC_URL }/pdfs/resume.pdf`);
  };

  // const handleRepositoryClick = () => {
  //   const { REACT_APP_REPOSITORY } = process.env;
  //   window.open(REACT_APP_REPOSITORY);
  // };

  const handleContactButtonClick = () => {
    const { REACT_APP_CONTACT_EMAIL } = process.env;
    window.open(`mailto:${ REACT_APP_CONTACT_EMAIL }`);
  };

  const handleVisibilityChange = isVisible => {
    setState(prev => ({
      ...prev,
      show: isVisible,
    }));
  };

  return (
    <Visibility once onChange={ handleVisibilityChange }>
      <Wrapper show={ state.show }>
        <Cover />
        <BottomWrapper>
          <LinkWrapper onClick={ handleLinkedInClick }>
            <LinkedInIcon />
            <Label>
              LINKEDIN
            </Label>
          </LinkWrapper>
          <LinkWrapper onClick={ handleResumeClick }>
            <ListIcon />
            <Label>
              PDF RESUME (may be outdated)
            </Label>
          </LinkWrapper>
          {/* <LinkWrapper onClick={ handleRepositoryClick }>
            <GitIcon />
            <Label>
              SOURCE CODE
            </Label>
          </LinkWrapper> */}
          <ContactButton onClick={ handleContactButtonClick }>
            MESSAGE ME
          </ContactButton>
        </BottomWrapper>
      </Wrapper>
    </Visibility>
  );
};

const Wrapper = styled(Card)`
  align-self: center;
  flex-grow: 1;
  margin-bottom: 30px;
  width: 100%;

  ${ mqDesktop`
    max-width: 400px;
    margin: 0;
    margin-right: 20px;
  ` }
`;

const BottomWrapper = styled.div`
  padding-top: 20px;
`;

const LinkWrapper = styled.div`
  align-self: flex-start;
  flex-direction: row;
  ${ buttonHovering }
  height: 30px;
`;

const LinkedInIcon = styled(LinkedIn)`
  align-self: center;
  color: ${ COLOR_5 };
  height: 20px;
  margin-right: 7px;
`;

const Label = styled(PrimaryLabel)`
  align-self: center;
  color: ${ COLOR_5 };
  font-size: 12px;
  line-height: 12px;
`;

const ListIcon = styled(LinkedInIcon).attrs({
  as: ListAlt,
})``;

// const GitIcon = styled(LinkedInIcon).attrs({
//   as: GitAlt,
// })``;

const ContactButton = styled(PrimaryButton)`
  align-self: center;
  margin-top: 20px;
`;

export default memo(Info, deepCompare);
