
import React, { memo } from 'react';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import Description from './description/Description';
import { Creators as HomePageReducerActions } from '../../../reducers/page/home/home.page.reducer';
import Info from './info/Info';
import Visibility from '../../../hocs/visibility/Visibility';
import { deepCompare } from '../../../utils/object/object.util';
import { mqDesktop } from '../../../styled/helpers/helpers.styled';

export const ID = 'about_section';

const About = () => {

  const { updateHomePage } = bindActionCreators({
    updateHomePage: HomePageReducerActions.updateHomePage,
  }, useDispatch());

  const handleVisibilityChange = isVisible => {
    updateHomePage({
      sectionVisibility: {
        about: isVisible,
      },
    });
  };

  return (
    <Visibility onChange={ handleVisibilityChange }>
      <Wrapper>
        <Info parentId={ ID } />
        <Description parentId={ ID } />
      </Wrapper>
    </Visibility>
  );
};

const Wrapper = styled.section.attrs({
  id: ID,
})`
  width: 100%;
  margin-bottom: 30px;

  ${ mqDesktop`
    flex-direction: row;
    justify-content: center;
  ` }
`;

export default memo(About, deepCompare);
