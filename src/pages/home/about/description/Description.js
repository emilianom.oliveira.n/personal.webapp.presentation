
import React, { memo, useEffect, useRef, useState } from 'react';
import { AddressCard } from '@styled-icons/fa-regular';
import TypewriterEffect from 'typewriter-effect';
import styled from 'styled-components';

import Card from '../../../../components/card/Card';
import { SecondaryLabel } from '../../../../styled/labels/labels.styled';
import Shadow from '../../../../hocs/shadow/Shadow';
import Visibility from '../../../../hocs/visibility/Visibility';
import { deepCompare } from '../../../../utils/object/object.util';
import { mqDesktop } from '../../../../styled/helpers/helpers.styled';

const Description = () => {

  const ref = useRef({
    typewriter: null,
  });

  const [ state, setState ] = useState({
    show: false,
  });

  useEffect(() => {
    const { typewriter } = ref.current;
    if(!state.show || !typewriter) return;
    startTyping();
  }, [
    state.show,
  ]);

  const handleTypewriter = typewriter => {
    ref.current.typewriter = typewriter;
    typewriter.pauseFor(2000)
      .typeString('"I\'m the kind of developer ')
      .typeString('that is passionate about processes, ')
      .pauseFor(250)
      .typeString('innovative projects ')
      .pauseFor(250)
      .typeString('or and beautiful UIs. ')
      .pauseFor(500)
      .typeString('Well thought ideas to me are almost ')
      .typeString('impossible to refuse.')
      .pauseFor(1000)
      .typeString('<br /><br />')
      .pauseFor(500)
      .typeString('I\'ve been working with Javascript f')
      .typeString('or 5 years, ')
      .pauseFor(250)
      .typeString('specially React, ')
      .pauseFor(250)
      .typeString('being familiar with most of the relev')
      .typeString('ant ecosystem (')
      .pauseFor(250)
      .typeString('Hooks, ')
      .pauseFor(250)
      .typeString('Mobx, ')
      .pauseFor(250)
      .typeString('Redux, ')
      .pauseFor(250)
      .typeString('Saga, ')
      .pauseFor(250)
      .typeString('Styled Components...')
      .pauseFor(250)
      .typeString('). ')
      .pauseFor(1000)
      .typeString('We can also include React Native, ')
      .pauseFor(250)
      .typeString('NodeJs and MongoDB on the list since ')
      .typeString('that\'s what have been my stack for t')
      .typeString('he past 2+ years.')
      .pauseFor(1000)
      .typeString('<br /><br />')
      .pauseFor(500)
      .typeString('Welcome to my page!')
      .pauseFor(1000)
      .typeString('<br /><br />')
      .pauseFor(500)
      .typeString('Feel free to send me a message. ')
      .pauseFor(500)
      .typeString('I\'d be glad to hear from you!" ');
  };

  const startTyping = () => {
    const { typewriter } = ref.current;
    typewriter.start();
  };

  const handleVisibilityChange = isVisible => {
    setState(prev => ({
      ...prev,
      show: isVisible,
    }));
  };

  return (
    <Visibility once onChange={ handleVisibilityChange }>
      <Wrapper show={ state.show }>
        <Shadow>
          <Sentence>
            &quot;I&apos;m the kind of developer that is passionate about processes, innovative projects or and beautiful UIs. Well thought ideas to me are almost impossible to refuse.
            <br />
            <br />
            I&apos;ve been working with Javascript for 4+ years, specially React, being familiar with most of the relevant ecosystem (Hooks, Redux, Saga, Styled Components...). We can also include React Native, NodeJs and MongoDB on the list since that&apos;s what have been my stack for the past 2 years, give or take.
            <br />
            <br />
            Welcome to my page!
            <br />
            <br />
            Feel free to send me a message. I&apos;d be glad to hear from you!&quot;
          </Sentence>
          <DesktopSentece>
            <DTypewriterEffect onInit={ handleTypewriter } />
          </DesktopSentece>
        </Shadow>
      </Wrapper>
    </Visibility>
  );
};

const Wrapper = styled(Card).attrs({
  icon: AddressCard,
  title: 'about me',
})`
  width: 100%;

  ${ mqDesktop`
    flex-grow: 1;
    width: 1px;
  ` }
`;

const Sentence = styled(SecondaryLabel).attrs({
  as: 'span',
})`
  display: inline-block;
  font-style: italic;
  margin-right: 5px;
  line-height: 20px;

  ${ mqDesktop`
    display: none;
  ` }
`;

const DesktopSentece = styled(Sentence)`
  display: none;
  ${ mqDesktop`
    display: flex;
  ` }
`;

const DTypewriterEffect = styled(TypewriterEffect).attrs({
  options: {
    delay: 10,
  },
})``;

export default memo(Description, deepCompare);
