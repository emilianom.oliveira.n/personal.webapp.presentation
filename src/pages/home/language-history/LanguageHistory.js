
import React, { memo, useState } from 'react';
import { ChartBar } from '@styled-icons/fa-regular';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import Card from '../../../components/card/Card';
import { Creators as HomePageReducerActions } from '../../../reducers/page/home/home.page.reducer';
import { LANGUAGE_HISTORY } from './constants/language-history.constants';
import LineChart from '../../../components/line-chart/LineChart';
import Visibility from '../../../hocs/visibility/Visibility';
import { bindActionCreators } from 'redux';
import { deepCompare } from '../../../utils/object/object.util';

export const ID = 'language_history_section';

const LanguageHistory = () => {

  const { updateHomePage } = bindActionCreators({
    updateHomePage: HomePageReducerActions.updateHomePage,
  }, useDispatch());

  const [ state, setState ] = useState({
    show: false,
  });

  const handleVisibilityChange = isVisible => {
    updateHomePage({
      sectionVisibility: {
        languageHistory: isVisible,
      },
    });

    if(!isVisible) return;
    setState(prev => ({
      ...prev,
      show: isVisible,
    }));
  };

  return (
    <Wrapper>
      <Visibility onChange={ handleVisibilityChange }>
        <LHCard show={ state.show }>
          <ContentWrapper>
            <LineChart data={ LANGUAGE_HISTORY } />
          </ContentWrapper>
        </LHCard>
      </Visibility>
    </Wrapper>
  );
};

const Wrapper = styled.section.attrs({
  id: ID,
})`
  margin-bottom: 30px;
`;

const LHCard = styled(Card).attrs({
  icon: ChartBar,
  title: 'language history',
})``;

const ContentWrapper = styled.div`
  height: 400px;
  width: 100%;
`;

export default memo(LanguageHistory, deepCompare);
