
const JAVASCRIPT = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 1,
  }, {
    x: 2018,
    y: 2,
  }, {
    x: 2019,
    y: 3,
  }, {
    x: 2020,
    y: 4,
  }, {
    x: 2021,
    y: 5,
  }, {
    x: 2022,
    y: 5.25,
  }],
  id: 'JavaScript',
};

const DART = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 0,
  }, {
    x: 2021,
    y: 0,
  }, {
    x: 2022,
    y: 0.25,
  }],
  id: 'Dart',
};

const TYPESCRIPT = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0.75,
  }, {
    x: 2019,
    y: 1.75,
  }, {
    x: 2020,
    y: 1.75,
  }, {
    x: 2021,
    y: 1.75,
  }, {
    x: 2022,
    y: 2,
  }],
  id: 'Typescript',
};

const CSHARP = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0.25,
  }, {
    x: 2019,
    y: 0.25,
  }, {
    x: 2020,
    y: 0.25,
  }, {
    x: 2021,
    y: 0.25,
  }, {
    x: 2022,
    y: 0.25,
  }],
  id: 'C#',
};

const JAVA = {
  data: [{
    x: 2016,
    y: 0,
  }, {
    x: 2017,
    y: 0,
  }, {
    x: 2018,
    y: 0,
  }, {
    x: 2019,
    y: 0,
  }, {
    x: 2020,
    y: 0,
  }, {
    x: 2021,
    y: 0.5,
  }, {
    x: 2022,
    y: 0.5,
  }],
  id: 'Java',
};

export const LANGUAGE_HISTORY = [
  CSHARP,
  DART,
  JAVA,
  JAVASCRIPT,
  TYPESCRIPT,
].sort((a, b) => {
  const first = a.data[a.data.length - 1].y;
  const second = b.data[b.data.length - 1].y;
  return first - second;
});
