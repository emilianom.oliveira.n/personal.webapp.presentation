
import React, { memo } from 'react';

import { backgrounder, mqTablet } from '../../../../styled/helpers/helpers.styled';
import { PrimaryLabel } from '../../../../styled/labels/labels.styled';
import { deepCompare } from '../../../../utils/object/object.util';
import propTypes from 'prop-types';
import styled from 'styled-components';

const Tool = ({
  thumbnail,
  label,
}) => (
  <Wrapper>
    <Thumbnail url={ thumbnail } />
    <Label>
      { label }
    </Label>
  </Wrapper>
);

const Wrapper = styled.div`
  flex-direction: row;
  margin-bottom: 20px;
  width: 100%;
  &:last-child {
    margin-bottom: 0;
  }

  ${ mqTablet`
    width: 33%;
    align-self: center;
    margin: 0;
  ` }
`;

const Thumbnail = styled.div`
  align-self: center;
  border-radius: 100%;
  height: 40px;
  margin-right: 10px;
  width: 40px;
  ${ ({ url }) => backgrounder({ url }) }
`;

const Label = styled(PrimaryLabel)`
  align-self: center;
  font-size: 18px;
`;

Tool.propTypes = {
  label: propTypes.string,
  thumbnail: propTypes.string,
};

Tool.defaultProps = { };

export default memo(Tool, deepCompare);
