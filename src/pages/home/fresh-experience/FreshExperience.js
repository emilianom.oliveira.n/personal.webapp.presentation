
import React, { Fragment, memo, useState } from 'react';

import { Star } from '@styled-icons/fa-regular';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import Card from '../../../components/card/Card';
import { FRESH_EXPERIENCE } from './constants/fresh-experience.constants';
import { Creators as HomePageReducerActions } from '../../../reducers/page/home/home.page.reducer';
import Tool from './tool/Tool';
import Visibility from '../../../hocs/visibility/Visibility';
import { deepCompare } from '../../../utils/object/object.util';
import { mqTablet } from '../../../styled/helpers/helpers.styled';

export const ID = 'fresh_experience_section';

const FreshExperience = () => {

  const { updateHomePage } = bindActionCreators({
    updateHomePage: HomePageReducerActions.updateHomePage,
  }, useDispatch());

  const [ state, setState ] = useState({
    show: false,
  });

  const handleVisibilityChange = isVisible => {
    updateHomePage({
      sectionVisibility: {
        freshExperience: isVisible,
      },
    });

    if(!isVisible) return;
    setState(prev => ({
      ...prev,
      show: isVisible,
    }));
  };

  return (
    <Wrapper>
      <Visibility onChange={ handleVisibilityChange }>
        <FECard show={ state.show }>
          <ListWrapper>
            { FRESH_EXPERIENCE.map((t, index) => (
              <Fragment key={ t.label }>
                <Tool
                  key={ t.label }
                  label={ t.label }
                  thumbnail={ t.thumbnail }
                />
                { (index + 1) % 3 === 0 && <Spacer /> }
              </Fragment>
            )) }
          </ListWrapper>
        </FECard>
      </Visibility>
    </Wrapper>
  );
};

const Wrapper = styled.section.attrs({
  id: ID,
})``;

const FECard = styled(Card).attrs({
  icon: Star,
  title: 'fresh experience',
})`
  margin-bottom: 30px;
`;

const ListWrapper = styled.div`
  ${ mqTablet`
    flex-wrap: wrap;
    flex-direction: row;
  ` }
`;

const Spacer = styled.div`
  display: none;
  ${ mqTablet`
    display: block;
    height: 20px;
    width: 100%;
  ` }
`;

export default memo(FreshExperience, deepCompare);
