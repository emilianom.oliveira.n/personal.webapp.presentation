
const { PUBLIC_URL } = process.env;

export const FRESH_EXPERIENCE = [{
  label: 'JavaScript',
  thumbnail: `${ PUBLIC_URL }/images/javascript-256.jpg`,
}, {
  label: 'React JS',
  thumbnail: `${ PUBLIC_URL }/images/react-js-256.jpg`,
}, {
  label: 'React Native',
  thumbnail: `${ PUBLIC_URL }/images/react-native-256.jpg`,
}, {
  label: 'Node JS',
  thumbnail: `${ PUBLIC_URL }/images/node-js-256.jpg`,
}, {
  label: 'MongoDB',
  thumbnail: `${ PUBLIC_URL }/images/mongodb-256.jpg`,
}];
