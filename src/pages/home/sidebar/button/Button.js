
import { Building, ChartBar, Star, UserCircle } from '@styled-icons/fa-regular';
import React, { memo } from 'react';
import styled, { css } from 'styled-components';
import propTypes from 'prop-types';
import { transitions } from 'polished';

import { COLOR_3, COLOR_6 } from '../../../../styled/variables/variables.styled';
import { deepCompare } from '../../../../utils/object/object.util';
import { dim } from '../../../../styled/helpers/helpers.styled';
import { pointer } from '../../../../styled/blocks/blocks.styled';

export const TYPES = {
  BUILDING: 'BUILDING',
  CHART: 'CHART',
  STAR: 'STAR',
  USER: 'USER',
};

const Button = ({
  isActive,
  isVisible,
  onClick,
  type,
}) => {

  let Icon;
  switch (type) {
    case TYPES.BUILDING: Icon = BuildingIcon; break;
    case TYPES.CHART: Icon = ChartIcon; break;
    case TYPES.USER: Icon = UserIcon; break;
    case TYPES.STAR: Icon = StarIcon; break;
    default: throw new Error('Unknown type.');
  }

  return (
    <Wrapper
      onClick={ onClick }
      isActive={ isActive }
    >
      <Bar isActive={ isActive } />
      <IconWrapper isActive={ isActive }>
        <Icon
          isActive={ isActive }
          isVisible={ isVisible }
        />
      </IconWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  flex-direction: row;
  ${ pointer }
  height: 60px;
  justify-content: center;
  width: 100%;

  ${ ({ isActive }) => isActive && (
    Wrapper.styleBlocks.active
  ) }

  ${ transitions([ 'background-color' ], 'ease .3s') }
  &:hover {
    ${ () => Wrapper.styleBlocks.active }
    & > :first-child {
      width: 10px;
    }
    & > :last-child > svg {
      ${ () => UserIcon.styleBlocks.active }
    }
  }
`;
Wrapper.styleBlocks = {
  active: css`
    ${ dim({
      amount: 0.15,
      color: COLOR_3,
    }) }
  `,
};

const Bar = styled.div`
  background-color: ${ COLOR_3 };
  height: 100%;
  
  width: 0;
  ${ transitions([ 'width' ], 'ease .1s') }
  ${ ({ isActive }) => isActive && css`
    width: 5px;
  ` }
`;

const IconWrapper = styled.div`
  flex-grow: 1;
  height: 100%;
  justify-content: center;
  width: 1px;
`;

const UserIcon = styled(UserCircle)`
  align-self: center;
  color: ${ COLOR_6 };
  height: 20px;
  
  ${ transitions([ 'color', 'ease .1s' ]) }
  ${ ({ isActive, isVisible }) => (isActive || isVisible) && (
    UserIcon.styleBlocks.active
  ) }
`;
UserIcon.styleBlocks = {
  active: css`
    color: ${ COLOR_3 };
  `,
};

const StarIcon = styled(UserIcon).attrs({
  as: Star,
})``;

const ChartIcon = styled(UserIcon).attrs({
  as: ChartBar,
})``;

const BuildingIcon = styled(UserIcon).attrs({
  as: Building,
})``;

Button.propTypes = {
  isActive: propTypes.bool,
  isVisible: propTypes.bool,
  onClick: propTypes.func,
  type: propTypes.oneOf([
    TYPES.USER,
    TYPES.BUILDING,
    TYPES.CHART,
    TYPES.STAR,
  ]),
};

Button.defaultProps = { };

export default memo(Button, deepCompare);
