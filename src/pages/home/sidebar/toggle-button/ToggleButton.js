
import React, { memo } from 'react';
import styled, { css } from 'styled-components';
import { KeyboardArrowRight } from '@styled-icons/material-rounded';
import propTypes from 'prop-types';
import { transitions } from 'polished';

import { COLOR_1, COLOR_3 } from '../../../../styled/variables/variables.styled';
import { mqTablet, shadow } from '../../../../styled/helpers/helpers.styled';
import { deepCompare } from '../../../../utils/object/object.util';
import { pointer } from '../../../../styled/blocks/blocks.styled';

const ToggleButton = ({
  onClick,
  isActive,
}) => (
  <Wrapper onClick={ onClick }>
    <Icon isActive={ isActive } />
  </Wrapper>
);

const Wrapper = styled.div`
  background: red;
  background-color: ${ COLOR_1 };
  border-radius: 0 5px 5px 0;
  height: 40px;
  margin-bottom: 20px;
  margin-left: calc(100% - 1px);
  margin-top: -60px;
  width: 40px;
  ${ pointer }
  ${ shadow({
    direction: 'bottom-right',
    size: 4,
  }) }

  ${ mqTablet`
    display: none;
  ` }
`;

const Icon = styled(KeyboardArrowRight)`
  color: ${ COLOR_3 };
  height: 100%;

  ${ transitions([ 'transform' ], 'ease .3s') }
  ${ ({ isActive }) => isActive && css`
    transform: rotate(-180deg);
  ` }
`;

ToggleButton.propTypes = {
  isActive: propTypes.bool,
  onClick: propTypes.func,
};

ToggleButton.defaultProps = { };

export default memo(ToggleButton, deepCompare);
