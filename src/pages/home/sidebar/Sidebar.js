
import React, { memo, useState } from 'react';
import styled, { css } from 'styled-components';
import { transitions } from 'polished';
import { useSelector } from 'react-redux';

import Button, { TYPES as BUTTON_TYPES } from './button/Button';
import { mqTablet, shadow } from '../../../styled/helpers/helpers.styled';
import { ID as ABOUT_ID } from '../about/About';
import { COLOR_1 } from '../../../styled/variables/variables.styled';
import { ID as FRESH_EXPERIENCE_ID } from '../fresh-experience/FreshExperience';
import { ID as JOB_HISTORY_ID } from '../job-history/JobHistory';
import { ID as LANGUAGE_HISTORY_ID } from '../language-history/LanguageHistory';
import ToggleButton from './toggle-button/ToggleButton';
import { deepCompare } from '../../../utils/object/object.util';
import { scrollToElement } from '../../../utils/scrolling/scrolling.util';

const Sidebar = () => {

  const {
    about,
    freshExperience,
    jobHistory,
    toolingHistory,
    languageHistory,
  } = useSelector(state => ({
    about: state.page.home.sectionVisibility.about,
    freshExperience: state.page.home.sectionVisibility.freshExperience,
    jobHistory: state.page.home.sectionVisibility.jobHistory,
    languageHistory: state.page.home.sectionVisibility.languageHistory,
    toolingHistory: state.page.home.sectionVisibility.toolingHistory,
  }));

  const [ state, setState ] = useState({
    activeButton: null,
    isActive: false,
  });

  const handleButtonClick = buttonType => {

    setState(prev => ({
      ...prev,
      activeButton: buttonType,
    }));

    const isMobile = window.innerWidth < 768;
    setTimeout(() => {
      if(isMobile) setState(prev => ({
        ...prev,
        isActive: false,
      }));

      setTimeout(() => {
        let sectionId;
        switch (buttonType) {
          case BUTTON_TYPES.USER:
            sectionId = ABOUT_ID;
            break;
          case BUTTON_TYPES.STAR:
            sectionId = FRESH_EXPERIENCE_ID;
            break;
          case BUTTON_TYPES.CHART:
            sectionId = LANGUAGE_HISTORY_ID;
            break;
          case BUTTON_TYPES.BUILDING:
            sectionId = JOB_HISTORY_ID;
            break;
          default: throw new Error('Unknown button.');
        }

        scrollToElement({
          element: sectionId,
        });
      }, isMobile ? 500 : 250);
    }, isMobile ? 500 : 0);
  };

  const handleToggleClick = () => {
    setState(prev => ({
      ...prev,
      isActive: !state.isActive,
    }));
  };

  return (
    <Wrapper isActive={ state.isActive }>
      <ButtonList isActive={ state.isActive }>
        { Sidebar.BUTTONS.map(b => {

          let isVisible;
          switch (b.type) {
            case BUTTON_TYPES.USER: isVisible = about; break;
            case BUTTON_TYPES.STAR: isVisible = freshExperience; break;
            case BUTTON_TYPES.CHART: {
              isVisible = languageHistory || toolingHistory;
              break;
            }
            case BUTTON_TYPES.BUILDING: isVisible = jobHistory; break;
            default: isVisible = false; break;
          }

          return (
            <Button
              key={ b.type }
              type={ b.type }
              isActive={ b.type === state.activeButton && isVisible }
              isVisible={ isVisible }
              onClick={ () => handleButtonClick(b.type) }
            />
          );
        }) }
      </ButtonList>
      <ToggleButton
        onClick={ handleToggleClick }
        isActive={ state.isActive }
      />
    </Wrapper>
  );
};

Sidebar.BUTTONS = [{
  type: BUTTON_TYPES.USER,
}, {
  type: BUTTON_TYPES.STAR,
}, {
  type: BUTTON_TYPES.CHART,
}, {
  type: BUTTON_TYPES.BUILDING,
}];

const Wrapper = styled.div`
  background-color: ${ COLOR_1 };
  border-radius: 0 0 10px 0;
  height: calc(100% - 80px);
  left: -70px;
  opacity: .5;
  position: fixed;
  ${ shadow({
    direction: 'bottom-right',
    size: 4,
  }) }

  top: 60px;
  width: 80px;
  ${ transitions([ 'left', 'opacity' ], 'ease .3s') }
  ${ ({ isActive }) => isActive && css`
    left: 0;
    opacity: 1;
  ` }

  ${ mqTablet`
    top: 80px;
    height: calc(100% - 100px);
    left: 0;
    opacity: 1;
  ` }
`;

const ButtonList = styled.div`
  background-color: ${ COLOR_1 };
  border-radius: 0 0 10px 0;
  flex-grow: 1;
  height: 1px;
  opacity: 0;
  padding-top: 20px;
  
  width: 100%;
  ${ transitions([ 'opacity' ], 'ease .3s') }
  ${ ({ isActive }) => isActive && css`
    opacity: 1;
  ` }

  ${ mqTablet`
    opacity: 1;
  ` }
`;

export default memo(Sidebar, deepCompare);
