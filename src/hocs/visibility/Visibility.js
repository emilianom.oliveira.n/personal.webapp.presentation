
import React, { memo, useRef } from 'react';

import VisibilitySensor from 'react-visibility-sensor';
import { deepCompare } from '../../utils/object/object.util';
import propTypes from 'prop-types';

const Visibility = ({
  once,
  children,
  onChange,
  offset,
}) => {

  const ref = useRef({
    lastState: false,
  });

  // DO NOT use the 'active' property to disable
  // the sensor. This will lead to unnecessary re
  // -renders.
  const handleChange = isVisible => {
    const { lastState } = ref.current;
    if(once && lastState) return;
    if(lastState === isVisible) return;

    ref.current.lastState = isVisible;
    if(onChange) onChange(isVisible);
  };

  return (
    <Wrapper
      onChange={ handleChange }
      partialVisibility
      delayedCall
      intervalDelay={ 1000 }
      minTopValue={ offset }
    >
      { children }
    </Wrapper>
  );
};

const Wrapper = VisibilitySensor;

Visibility.propTypes = {
  children: propTypes.any,
  offset: propTypes.number,
  onChange: propTypes.func,
  once: propTypes.bool,
};

Visibility.defaultProps = {
  offset: 125,
  once: false,
};

export default memo(Visibility, deepCompare);
