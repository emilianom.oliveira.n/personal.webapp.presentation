
import React, { memo } from 'react';

import { deepCompare } from '../../utils/object/object.util';
import propTypes from 'prop-types';
import root from 'react-shadow/styled-components';

const Shadow = ({ children }) => (
  <Wrapper>
    { children }
  </Wrapper>
);

const Wrapper = root.div;

Shadow.propTypes = {
  children: propTypes.any,
};

Shadow.defaultProps = { };

export default memo(Shadow, deepCompare);
