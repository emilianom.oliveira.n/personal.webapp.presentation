
import React, { memo } from 'react';
import { Provider } from 'react-redux';
import propTypes from 'prop-types';

import { deepCompare } from '../../utils/object/object.util';
import store from '../../store/store';

const StoreProvider = ({ children }) => (
  <Provider store={ store }>
    { children }
  </Provider>
);

StoreProvider.propTypes = {
  children: propTypes.any,
};

StoreProvider.defaultProps = { };

export default memo(StoreProvider, deepCompare);
