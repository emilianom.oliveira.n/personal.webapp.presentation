
import React, { memo } from 'react';
import styled from 'styled-components';

import { mqTablet, shadow } from '../../styled/helpers/helpers.styled';
import Brand from './brand/Brand';
import { COLOR_1 } from '../../styled/variables/variables.styled';
import { PrimaryButton } from '../../styled/buttons/buttons.styled';
import { deepCompare } from '../../utils/object/object.util';

const Header = () => {

  const handleContactButtonClick = () => {
    const { REACT_APP_CONTACT_EMAIL } = process.env;
    window.open(`mailto:${ REACT_APP_CONTACT_EMAIL }`);
  };

  return (
    <Wrapper>
      <Brand />
      <ContactButton onClick={ handleContactButtonClick }>
        message me
      </ContactButton>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background-color: ${ COLOR_1 };
  flex-direction: row;
  height: 60px;
  justify-content: space-between;
  padding: 0 20px;
  position: fixed;
  width: 100vw;
  ${ shadow({ }) }

  ${ mqTablet`
    height: 80px;
  ` }
`;

const ContactButton = styled(PrimaryButton)`
  display: none;
  ${ mqTablet`
    display: block;
    align-self: center;
  ` }
`;

export default memo(Header, deepCompare);
