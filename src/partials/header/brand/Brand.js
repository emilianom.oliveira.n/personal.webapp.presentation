
import React, { memo } from 'react';
import styled from 'styled-components';

import { PrimaryLabel, SecondaryLabel } from '../../../styled/labels/labels.styled';
import { ID as ABOUT_ID } from '../../../pages/home/about/About';
import { ProfilePictureSmall } from '../../../styled/components/components.styled';
import { buttonHovering } from '../../../styled/blocks/blocks.styled';
import { deepCompare } from '../../../utils/object/object.util';
import { scrollToElement } from '../../../utils/scrolling/scrolling.util';

const Brand = () => {

  const handleClick = () => {
    scrollToElement({
      element: ABOUT_ID,
    });
  };

  return (
    <Wrapper onClick={ handleClick }>
      <Picture />
      <NameWrapper>
        <Name>
          Emiliano Oliveira
        </Name>
        <Profession>
          Software Engineer
        </Profession>
      </NameWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  align-self: center;
  flex-direction: row;
  ${ buttonHovering() }
`;

const Picture = styled(ProfilePictureSmall)`
  align-self: center;
  height: 40px;
  margin-right: 10px;
  width: 40px;
`;

const NameWrapper = styled.div`
  align-self: center;
`;

const Name = styled(PrimaryLabel)``;

const Profession = styled(SecondaryLabel)`
  font-size: 12px;
`;

export default memo(Brand, deepCompare);
