
export const COLOR_1 = '#FFFFFF';
export const COLOR_2 = '#F4F6FC';
export const COLOR_3 = '#3261FB';
export const COLOR_4 = '#333333';
export const COLOR_5 = '#717171';
export const COLOR_6 = '#B6BDCC';
export const COLOR_7 = '#E5E9F0';
