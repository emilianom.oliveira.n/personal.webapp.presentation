
import { COLOR_2 } from '../variables/variables.styled';
import { css } from 'styled-components';
import { mqTablet } from '../helpers/helpers.styled';
import { transitions } from 'polished';

export const pageBase = () => css`
  background-color: ${ COLOR_2 };
  min-height: 100vh;
  padding: 80px 20px;

  ${ mqTablet`
    padding: 110px 50px;
  ` }
`;

export const pointer = () => css`
  &, * {
    cursor: pointer;
  }
`;

export const buttonHovering = () => css`
  opacity: 1;
  transform: scale(1);
  ${ pointer }

  ${ transitions([ 'transform', 'opacity' ], 'ease .3s') }
  &:hover {
    opacity: 0.7;
    transform: scale(1.05);
  }

  ${ mqTablet`
    &:hover {
      transform: scale(1.1);
      opacity: 0.7;
    }
  ` }
`;
