
import { COLOR_4, COLOR_5 } from '../variables/variables.styled';
import styled from 'styled-components';

export const PrimaryLabel = styled.label`
  color: ${ COLOR_4 };
  font-size: 16px;
  letter-spacing: 1px;
`;

export const SecondaryLabel = styled(PrimaryLabel)`
  color: ${ COLOR_5 };
`;
