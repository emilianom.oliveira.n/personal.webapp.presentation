
import { COLOR_1, COLOR_3 } from '../variables/variables.styled';
import { buttonHovering } from '../blocks/blocks.styled';
import styled from 'styled-components';

export const PrimaryButton = styled.button`
  background-color: ${ COLOR_3 };
  border-radius: 45px;
  border-radius: 3px;
  color: ${ COLOR_1 };
  font-size: 12px;
  font-weight: bold;
  height: 40px;
  line-height: 12px;
  padding: 0 20px;
  text-transform: uppercase;
  width: fit-content;
  ${ buttonHovering() }
`;
