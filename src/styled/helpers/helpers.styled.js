
import { parseToRgb, rgba } from "polished";
import { css } from "styled-components";

import { COLOR_4 } from "../variables/variables.styled";

export const backgrounder = ({ url }) => css`
  background-image: url('${ url }');
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const dim = ({ amount = 0.7, color = COLOR_4 }) => css`
  background-color: ${ rgba({
    ...parseToRgb(color),
    alpha: amount,
  }) };
`;

export const mqTablet = (...style) => css`
  @media (min-width: 768px) {
    ${ css(...style) };
  }
`;

export const mqDesktop = (...style) => css`
  @media (min-width: 1080px) {
    ${ css(...style) };
  }
`;

export const shadow = ({
  direction = 'bottom',
  size = 5,
  opacity = 0.1,
}) => {

  let vertical = 0,
      horizontal = 0,
      spread = 0;
  const negative = -1;

  switch (direction) {
    case 'top': {
      vertical = negative * size;
      break;
    }
    case 'top-right': {
      vertical = negative * size;
      horizontal = size;
      break;
    }
    case 'right': {
      horizontal = size;
      break;
    }
    case 'bottom-right': {
      vertical = size;
      horizontal = size;
      break;
    }
    case 'bottom': {
      vertical = size;
      break;
    }
    case 'bottom-left': {
      vertical = size;
      horizontal = negative * size;
      break;
    }
    case 'left': {
      horizontal = negative * size;
      break;
    }
    case 'top-left': {
      vertical = negative * size;
      horizontal = negative * size;
      break;
    }
    case 'all': {
      spread = size;
      break;
    }
    default: throw new Error('Unknown direction.');
  }

  return css`
    box-shadow:
      ${ horizontal }px
      ${ vertical }px
      6px
      ${ spread }px
      rgba(0, 0, 0, ${ opacity });
  `;
};
