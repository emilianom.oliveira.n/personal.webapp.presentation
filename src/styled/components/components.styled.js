
import { COLOR_7 } from "../variables/variables.styled";
import styled from "styled-components";

const { PUBLIC_URL } = process.env;

export const ProfilePictureSmall = styled.img.attrs({
  src: `${ PUBLIC_URL }/images/profile-128.jpg`,
})`
  border-radius: 100%;
`;

export const ProfilePictureMedium = styled.img.attrs({
  src: `${ PUBLIC_URL }/images/profile-256.jpg`,
})`
  border-radius: 100%;
`;

export const Divider = styled.div`
  background-color: ${ COLOR_7 };
  height: 1px;
  margin: 20px 0;
  width: 100%;
`;
