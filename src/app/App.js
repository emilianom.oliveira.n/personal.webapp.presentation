import React, { StrictMode } from 'react';

import Routing from './routing/Routing';
import StoreProvider from '../hocs/store-provider/StoreProvider';
import { StyleResetter } from '../styled/global/global.styled';

const App = () => (
  <StrictMode>
    <StoreProvider>
      <StyleResetter />
      <Routing />
    </StoreProvider>
  </StrictMode>
);

export default App;
