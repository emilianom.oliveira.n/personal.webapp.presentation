
import React, { Fragment } from 'react';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from '../../partials/header/Header';
import Home from '../../pages/home/Home';

const Routing = () => (
  <Wrapper>
    <BrowserRouter>
      <Switch>
        <Route path="/" component={ Home } />
      </Switch>
    </BrowserRouter>
    <Header />
  </Wrapper>
);

const Wrapper = Fragment;

export default Routing;
