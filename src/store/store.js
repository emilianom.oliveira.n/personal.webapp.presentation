
import { applyMiddleware, createStore } from 'redux';

import reducer from '../reducers/reducers';

const logger = () => next => action => {
  if(process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line no-console
    console.log(`*** ACTION DISPATCHED: ${ action.type }`);
  }
  next(action);
};

const middlewares = [ logger ];

export const store = createStore(
  reducer,
  applyMiddleware(...middlewares)
);

export default store;
